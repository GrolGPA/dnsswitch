#!/usr/bin/python3
#
# python3.5 -m pip install cloudflare

import argparse
import json
import CloudFlare
from pprint import pprint


def parse_args():
    parser = argparse.ArgumentParser(description='Mail-related tool')
    parser.add_argument('-c', '--cmd', required=True, help="Choise of action. Available options: add - adding new DNS record to zone; update - updating ip address of DNS record; show - showing existed DNS records, clear - clearing cache of zone; del - deleting DNS record")
    parser.add_argument('-d', '--dns', required=True, help='Specify DNS name or group to switch', default=0)
    parser.add_argument('-s', '--server', required=False, help='Target server for switching')
    parser.add_argument('-o', '--options', required=False, help='Additional options. Available options: devmode, noproxy, second_dns, third_dns', default=0)
    parser.add_argument('-v', '--verbose', help='increase output verbosity', action="count", default=0)
    args = parser.parse_args()
    return args


def get_zones(cf, dns, zone_name):

    #Getting all zones list in CF account
    try:
        params = {'name': zone_name, 'per_page': 1}
    except IndexError:
        params = {'per_page': 50}
    zones = cf.zones.get(params=params)

    for zone in zones:

        zone_id = zone['id']
        zone_name = zone['name']
        return zone_id, zone_name

#Getting zone name and CloudFlare credentials
def get_dns_prop(config, domain):
    dns_params = dict()

    # def get_cfcred (name_cf):
    #     for j in range(len(name_cf)):
    #         cf_acc = name_cf[j]
    #         if cf_acc.get('name') == name_cf:
    #             dns_params["mail"] = cf_acc.get('mail')
    #             dns_params["api_key"] = cf_acc.get('api_key')
    #     return


    #Geting zone name and parameters
    for i in range(len(config["domains"])):

        domain_info = config["domains"][i]
        # vhosts_list.append(domain_info.get('name'))
        #Getting if main domen
        if domain_info.get('name') == domain:

            dns_params['zone'] = domain_info.get('name')
            dns_params['domain'] = domain_info.get('name')
            name_cf = domain_info.get('cf_acc')
            vhosts_list.append(dns_params['domain'])

            # Getting CloudFlare credentials
            for j in range(len(config["cf_accs"])):
                cf_acc = config["cf_accs"][j]
                if cf_acc.get('name') == name_cf:
                    dns_params["mail"] = cf_acc.get('mail')
                    dns_params["api_key"] = cf_acc.get('api_key')

        #Getting if subdomain
        elif domain.endswith(domain_info.get('name')):


            dns_params['zone'] = domain_info.get('name')
            dns_params['domain'] = domain
            vhosts_list.append(dns_params['domain'])

            name_cf = domain_info.get('cf_acc')

            # Getting CloudFlare credentials
            for j in range(len(config["cf_accs"])):
                cf_acc = config["cf_accs"][j]
                if cf_acc.get('name') == name_cf:
                    dns_params["mail"] = cf_acc.get('mail')
                    dns_params["api_key"] = cf_acc.get('api_key')

    return dns_params

#Showing all DNS records of zone
def show_dns(dns, zone_name, email, api_key):

    cf = CloudFlare.CloudFlare(email=email, token=api_key)
    zone = get_zones(cf, dns, zone_name)
    zone_id = zone[0]

    # Now reading all existing DNS records
    print('Read DNS records ...')
    try:
        dns_records = cf.zones.dns_records.get(zone_id)
    except CloudFlare.exceptions.CloudFlareAPIError as e:
        exit('/zones.dns_records.get %s - %d %s' % (zone_name, e, e))

    for dns_record in sorted(dns_records, key=lambda v: v['name']):
        print('\t%s %30s %6d %-5s %s ; proxied=%s proxiable=%s' % (
            dns_record['id'],
            dns_record['name'],
            dns_record['ttl'],
            dns_record['type'],
            dns_record['content'],
            dns_record['proxied'],
            dns_record['proxiable']
        ))

    print('')
    exit(0)

def switch(zones_list, ip, email, api_key, options, verbose):
    cf = CloudFlare.CloudFlare(email=email, token=api_key)

    for zone_name in zones_list:
        # query for the zone name and expect only one value back
        try:
            zones = cf.zones.get(params={'name': zone_name, 'per_page': 1})
        except CloudFlare.exceptions.CloudFlareAPIError as e:
            exit('/zones.get %d %s - api call failed' % (e, e))
        except Exception as e:
            exit('/zones.get - %s - api call failed' % (e))

        if len(zones) == 0:
            exit('No zones found')

        # extract the zone_id which is needed to process that zone
        zone = zones[0]
        zone_id = zone['id']

        # request the DNS records from that zone
        try:
            dns_records = cf.zones.dns_records.get(zone_id)
        except CloudFlare.exceptions.CloudFlareAPIError as e:
            exit('/zones/dns_records.get %d %s - api call failed' % (e, e))

        # print the results - first the zone name
        if verbose > 0: print ('Processing ', zone_name, zone_id)

        # then all the DNS records for that zone

        for dns_record in dns_records:
            r_name = dns_record['name']
            r_type = dns_record['type']
            r_value = dns_record['content']
            r_proxied = dns_record['proxied']
            r_id = dns_record['id']
            if vhosts_list and r_type == 'A' and r_name in vhosts_list:
                # if verbose > 0:
                # if verbose > 1: print(result)
                do_dns_update(cf, zone_name, zone_id, r_name, ip, r_type, options)
                print ('[x]', '\t', r_id, r_name, r_type, r_value, r_proxied)
            elif r_type == 'A':
                print ('[ ]', '\t', r_id, r_name, r_type, r_value, r_proxied)
            else:
                if verbose > 0: print ('\t', r_id, r_name, r_type, r_value, r_proxied)


def do_dns_update(cf, zone_name, zone_id, dns_name, ip_address, ip_address_type, options):

    try:
        params = {'name':dns_name, 'match':'all', 'type':ip_address_type}
        dns_records = cf.zones.dns_records.get(zone_id, params=params)
    except CloudFlare.exceptions.CloudFlareAPIError as e:
        exit('/zones/dns_records %s - %d %s - api call failed' % (dns_name, e, e))

    updated = False

    # update the record - unless it's already correct

    dns_count = 0
    for dns_record in dns_records:
        old_ip_address = dns_record['content']
        old_ip_address_type = dns_record['type']

        #Counter same DNS records
        last_dns = dns_record['name']

        if last_dns == dns_record['name']:
            dns_count += 1

        if (options == "second_dns") and (dns_count != 2):
            # we skip first DNS
            continue
        elif (options == "third_dns") and (dns_count < 3):
            # skip first and second dns records
            continue
        elif (options !="second_dns") and (options !="third_dns") and (dns_count > 1):
            # skips duplicate entries
            continue

        if ip_address_type not in ['A', 'AAAA']:
            # we only deal with A / AAAA records
            continue

        if ip_address_type != old_ip_address_type:
            # only update the correct address type (A or AAAA)
            # we don't see this becuase of the search params above
            print('IGNORED: %s %s ; wrong address family' % (dns_name, old_ip_address))
            continue

        if ip_address == old_ip_address:
            print('UNCHANGED: %s %s' % (dns_name, ip_address))
            updated = True
            continue

        # Yes, we need to update this record - we know it's the same address type

        dns_record_id = dns_record['id']
        dns_record = {
            'name':dns_name,
            'type':ip_address_type,
            'content':ip_address,
            'proxied':True
        }
        try:
            dns_record = cf.zones.dns_records.put(zone_id, dns_record_id, data=dns_record)
        except CloudFlare.exceptions.CloudFlareAPIError as e:
            exit('/zones.dns_records.put %s - %d %s - api call failed' % (dns_name, e, e))
        print('UPDATED: %s %s -> %s' % (dns_name, old_ip_address, ip_address))
        updated = True

    if updated:
        return


def create_dns (dns, zone_name, ip, email, api_key):

    cf = CloudFlare.CloudFlare(email=email, token=api_key)
    zone = get_zones(cf, dns, zone_name)
    zone_id = zone[0]
    # DNS records to create
    dns_record =  {'name': dns, 'type':'A', 'content':ip}

    print('Create DNS records ...')
        # Create DNS record
    try:
       r = cf.zones.dns_records.post(zone_id, data=dns_record)
    except CloudFlare.exceptions.CloudFlareAPIError as e:
        exit('/zones.dns_records.post %s %s - %d %s' % (zone_name, dns_record['name'], e, e))
    # Print respose info - they should be the same
    dns_record = r
    print('\t%s %30s %6d %-5s %s ; proxied=%s proxiable=%s' % (
        dns_record['id'],
        dns_record['name'],
        dns_record['ttl'],
        dns_record['type'],
        dns_record['content'],
        dns_record['proxied'],
        dns_record['proxiable']
    ))

    # set proxied flag to false - for example
    dns_record_id = dns_record['id']

    new_dns_record = {
        # Must have type/name/content (even if they don't change)
        'type':dns_record['type'],
        'name':dns_record['name'],
        'content':dns_record['content'],
        # now add new values you want to change
        'proxied':True
    }

    try:
        dns_record = cf.zones.dns_records.put(zone_id, dns_record_id, data=new_dns_record)
    except CloudFlare.exceptions.CloudFlareAPIError as e:
        exit('/zones/dns_records.put %d %s - api call failed' % (e, e))


    print('')
    exit(0)


#Clearing cache of selected zone
def clear_cache(dns, dns_params):

    cf = CloudFlare.CloudFlare(email=dns_params["mail"], token=dns_params["api_key"])
    zone = get_zones(cf, dns, dns_params["zone"])
    zone_id = zone[0]
    # --delete purge_everything = true
    try:
        true="true"
        bool(true)
        params = {'purge_everything': bool(true)}
        cache = cf.zones.purge_cache.delete(zone_id, data=params)
        if cache["id"]:
            print("Cache for domain \"", dns_params["zone"], "\" succesfully clearned")



    except CloudFlare.exceptions.CloudFlareAPIError as e:
        print('/zones/dns_records.get %d %s - api call failed' % (e, e))
        exit()



def del_zone(dns, dns_params, ip, verbose):
    cf = CloudFlare.CloudFlare(email=dns_params["mail"], token=dns_params["api_key"])
    try:
        zones = cf.zones.get(params={'name': dns_params["zone"]})
    except CloudFlare.exceptions.CloudFlareAPIError as e:
        print('/zones/dns_records.get %d %s - api call failed' % (e, e))
        exit()
    zone_info = zones[0]
    zone_id = zone_info["id"]
    if verbose > 0: print("Zone id: " + zone_id)

    try:
        dns_records = cf.zones.dns_records.get(zone_id, params={'name': dns})
    except CloudFlare.exceptions.CloudFlareAPIError as e:
        print('/zones/dns_records.get %d %s - api call failed' % (e, e))
        exit()

    #Checking server ip for deleting records
    fdel = bool()
    for dns_record in dns_records:
        dns_record_id = dns_record['id']
        try:
            if dns_record["content"] == ip:
                result = cf.zones.dns_records.delete(zone_id, dns_record_id)
                fdel = True
                print("DNS record " + dns + " pointed to ip: " + dns_record["content"] + " was deleted")
            elif ip == "all":
                result = cf.zones.dns_records.delete(zone_id, dns_record_id)
                fdel = True
                print("DNS record \"" + dns + "\" pointed to ip: " + dns_record["content"] + " was deleted")
        except CloudFlare.exceptions.CloudFlareAPIError as e:
            print('/zones/dns_records.get %d %s - api call failed' % (e, e))
            exit()

    if fdel == False:
        print ("No DNS records pointed to this server")
        exit()



def main():
    global vhosts_list
    vhosts_list = list()
    zone_list = list()


    try:
        with open('config/config.json') as data_file:
            config = json.load(data_file)
    except :
        print('ERROR: Specified file couldn\'t be opened')
        exit()


    args = parse_args()

    #Checking entered server
    if args.dns == 'all':
        print("Selected all domains")

    elif args.dns == 0:
        print("No domains selected")
        exit()

    else:

        domain = args.dns
        ip = args.server
        dns_params = get_dns_prop(config, domain)
        zone_list.append(dns_params["zone"])


    #Check action
    if args.cmd == "update":
        print ("Changing DNS record")
        switch(zone_list, ip, dns_params["mail"], dns_params["api_key"], args.options, args.verbose)

    elif args.cmd == "add":
        print ("Adding new DNS record for zone:", zone_list)
        create_dns(args.dns, dns_params["zone"], ip, dns_params["mail"], dns_params["api_key"])

    elif args.cmd == "clear":
        print ("Started clearing cache...")
        clear_cache(args.dns, dns_params)

    elif args.cmd == "del":
        if ip == False:
            print ("Enter IP address of server or parameter \"all\" for deleting DNS record")
            exit()
        else:
            del_zone(args.dns, dns_params, ip, args.verbose)

    elif args.cmd == "show":
        print ("Reading existing DNS records...")
        show_dns(args.dns, dns_params["zone"], dns_params["mail"], dns_params["api_key"])

    else:
        print ('Not entered or unknown action')
        exit()




    # zones = get_zones(dns_params["mail"], dns_params["api_key"])
    # print (zones)


if __name__ == '__main__':
    main()